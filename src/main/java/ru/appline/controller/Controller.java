package ru.appline.controller;

import org.springframework.web.bind.annotation.*;
import ru.appline.logic.Pet;
import ru.appline.logic.PetModel;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class Controller {
    private static final PetModel petModel = PetModel.getInstance();
    private static final AtomicInteger newId = new AtomicInteger(1);

    @PostMapping(value = "/createPet", consumes = "application/json")
    public String createPet(@RequestBody Pet pet) {
        petModel.add(pet, newId.getAndIncrement());
        if (petModel.getAll().size() == 1) {
            return "Pozdravlyaem, vi sozdali pervogo pitomca!";
        } else return "Vi sozdali pitomca";
    }

    @GetMapping(value = "/getAll", produces = "application/json")
    public Map<Integer, Pet> getAll() {
        return petModel.getAll();
    }
    /*
        {
            "id": 3
        }
    */
    @GetMapping(value = "/getPet", consumes = "application/json", produces = "application/json")
    public Pet getPet(@RequestBody Map<String, Integer> id) {
        return petModel.getFromList(id.get("id"));
    }

    @DeleteMapping(value = "/deletePet", consumes = "application/json", produces = "application/json")
    public String deletePet(Integer id) {
        petModel.Delete(id);
        return "Pitomec udalen";
    }
    @PutMapping(value = "/replacePet", consumes = "application/json", produces = "application/json")
    public Map<Integer, Pet> putPet(@RequestBody int id, Pet pet) {
        petModel.Put(id, pet);
        return petModel.getAll();
    }
}